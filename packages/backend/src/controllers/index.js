const { readFileSync, writeFileSync } = require('fs');
const readXlsxFile = require('read-excel-file/node');


const getExpert = (req, res) => {
  console.log('==== GET ==== Expert ====');
  const base = JSON.parse(readFileSync('./db/base.json').toString());
  res.send(base);
};

const saveExpert = (req, res) => {
  console.log('==== SAVE ==== Expert ====');

  writeFileSync('./db/base.json', JSON.stringify(req.body, null, '  '));
  res.sendStatus(200);
};

const getFromXLSX1 = (req, res) => {
  readXlsxFile('./db/base.xlsx', { sheet: 1 }).then((rows) => {
    console.log('==== GET ==== XLSX1 ====');

    res.send(rows);
  });
};

const getFromXLSX2 = (req, res) => {
  readXlsxFile('./db/base.xlsx', { sheet: 2 }).then((rows) => {
    console.log('==== GET ==== XLSX2 ====');
    const columns = [];
    for (let i = 1; i < rows[0].length; i++) {
      let test = [];
      for (let j = 0; j < rows.length; j++) {
        test.push(rows[j][i]);
      }
      columns.push(test);
      test = [];
    }
    res.send({ healthy: columns });
  });
};

const getFromXLSX3 = (req, res) => {
  readXlsxFile('./db/base.xlsx', { sheet: 3 }).then((rows) => {
    console.log('==== GET ==== XLSX3 ====');

    const columns = [];
    for (let i = 1; i < rows[0].length; i++) {
      let test = [];
      for (let j = 0; j < rows.length; j++) {
        test.push(rows[j][i]);
      }
      columns.push(test);
      test = [];
    }
    res.send({ illness: columns });
  });
};

module.exports = {
  getExpert,
  saveExpert,
  getFromXLSX1,
  getFromXLSX2,
  getFromXLSX3,
};
