'use strict';

const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');

const controller = require('./controllers');

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '1mb', extended: true }));

app.get('/expert', controller.getExpert);
app.post('/expert', controller.saveExpert);
app.get('/answers1', controller.getFromXLSX1);
app.get('/answers2', controller.getFromXLSX2);
app.get('/answers3', controller.getFromXLSX3);

app.listen(3000, () => {
  console.log(`App listening on port ${3000}! http://localhost:3000/`);
});
