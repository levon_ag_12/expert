export const SAVE_EXPERT_ASYNC = 'SAVE_EXPERT_ASYNC';
export const GET_EXPERT_ASYNC = 'GET_EXPERT_ASYNC';
export const GET_EXPERT_QUESTION_ASYNC = 'GET_EXPERT_QUESTION_ASYNC';
export const SET_EXPERT_TABLE = 'SET_EXPERT_TABLE';
export const CHANGE_CHECKBOX = 'CHANGE_CHECKBOX';
export const ADD_COLUMN = 'ADD_COLUMN';
export const ADD_ROW = 'ADD_ROW';
export const DELETE_COLUMN = 'DELETE_COLUMN';
export const DELETE_ROW = 'DELETE_ROW';
export const XLSX = 'XLSX';
export const ADD_XLSX = 'ADD_XLSX';

export const SUBMIT_BUTTON = {
  cancel: 'CANCEL',
  no: 'NO',
  reset: 'RESET',
  save: 'SAVE',
  start: 'START',
  yes: 'YES',
};
export const BUTTON = {
  find: {
    id: 'button-find',
    title: 'FIND',
    url: '/find',
  },
  learn: {
    id: 'button-learn',
    title: 'LEARN',
    url: '/learn',
  },
  lab2: {
    id: 'lab2',
    title: 'LAB2',
    url: '/lab2',
  }
};
