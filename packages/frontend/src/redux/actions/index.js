import { createAction } from 'redux-actions';

import {
  SAVE_EXPERT_ASYNC,
  GET_EXPERT_ASYNC,
  SET_EXPERT_TABLE,
  CHANGE_CHECKBOX,
  ADD_COLUMN,
  ADD_ROW,
  DELETE_COLUMN,
  DELETE_ROW,
  XLSX,
  ADD_XLSX,
} from '../../constants';

export const getExpert = createAction(GET_EXPERT_ASYNC);
export const saveExpert = createAction(SAVE_EXPERT_ASYNC);
export const setExpertTable = createAction(SET_EXPERT_TABLE);
export const changeCheckBox = createAction(CHANGE_CHECKBOX);
export const addColumn = createAction(ADD_COLUMN);
export const addRow = createAction(ADD_ROW);
export const deleteColumn = createAction(DELETE_COLUMN);
export const deleteRow = createAction(DELETE_ROW);

export const getXLSX = createAction(XLSX);
export const addXLSX = createAction(ADD_XLSX);
