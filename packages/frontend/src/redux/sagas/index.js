import { all } from 'redux-saga/effects';

import watchExpert from './expert';

export default function *() {
  yield all([
    watchExpert(),
  ]);
}
