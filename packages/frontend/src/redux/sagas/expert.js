/* eslint-disable func-style */
import { all, call, put, takeEvery } from 'redux-saga/effects';

import { setExpertTable, addXLSX } from '../actions';
import {
  SAVE_EXPERT_ASYNC,
  GET_EXPERT_ASYNC,
  XLSX,
} from '../../constants';
import { saveExpert, getExpert, getXLSX } from '../../requests';

function *saveExpertAsync({ payload }) {
  try {
    return yield call(saveExpert, payload);
  } catch (error) {
    console.log(error);
    return {};
  }
}

function *getExpertAsync() {
  try {
    const expert = yield call(getExpert);
    yield put(setExpertTable(expert));
  } catch (error) {
    console.log(error);
  }
}
function *getXLSXAsync() {
  try {
    const xlsx = yield call(getXLSX);
    yield put(addXLSX(xlsx));
  } catch (error) {
    console.log(error);
  }
}

export default function *() {
  yield all([
    yield takeEvery(SAVE_EXPERT_ASYNC, saveExpertAsync),
    yield takeEvery(GET_EXPERT_ASYNC, getExpertAsync),
    yield takeEvery(XLSX, getXLSXAsync),
  ]);
}
