import { handleActions } from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import omit from 'lodash/omit';

import {
  ADD_COLUMN,
  ADD_ROW,
  SET_EXPERT_TABLE,
  CHANGE_CHECKBOX,
  DELETE_COLUMN,
  DELETE_ROW,
} from '../../constants';

const defaultState = {};

export default reduceReducers(
  (state) => state || defaultState,

  handleActions({
    [ADD_COLUMN]: (state, { payload }) => {
      const newState = { ...state };
      Object.keys(state).forEach(item => {
        newState[item][payload] = false;
      });
      return {
        ...state,
        ...newState
      };
    },

    [ADD_ROW]: (state, { payload }) => ({
      ...state,
      [payload]: Object.keys(state[Object.keys(state)[0]]).reduce((acc, val) => {
        acc[val] = false;
        return acc;
      }, {}),
    }),

    [CHANGE_CHECKBOX]: (state, { payload: { key, value } }) => ({
      ...state,
      [key]: {
        ...state[key],
        [value]: !state[key][value],
      }
    }),

    [DELETE_COLUMN]: (state, { payload }) => {
      const newState = Object.entries(state).reduce((acc, [key, val]) => {
        acc[key] = omit(val, payload);
        return acc;
      }, {});
      return {
        ...state,
        ...newState
      };
    },

    [DELETE_ROW]: (state, { payload }) => ({
      ...omit(state, payload)
    }),

    [SET_EXPERT_TABLE]: (state, { payload }) => ({
      ...state,
      ...payload,
    }),


  }, defaultState)
);
