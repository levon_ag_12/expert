import { handleActions } from 'redux-actions';
import reduceReducers from 'reduce-reducers';

import {
  ADD_XLSX,
} from '../../constants';

const defaultState = {};

export default reduceReducers(
  (state) => state || defaultState,

  handleActions({

    [ADD_XLSX]: (state, { payload }) => ({
      ...state,
      ...payload,
    })

  }, defaultState)
);
