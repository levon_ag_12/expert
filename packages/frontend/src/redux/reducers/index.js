import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import expert from './expert';
import xlsx from './xlsx';

const rootReducer = combineReducers({
  expert,
  routing,
  xlsx,
});

export default rootReducer;
