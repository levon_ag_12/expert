/* eslint-disable */
export default {
    BACKEND_HOST: 'http://localhost:3000',
    DEFAULT_RANKED_SIZE: process.env.DEFAULT_RANKED_SIZE || 10,
    DEFAULT_SMALL_RANKED_INDENT: process.env.DEFAULT_SMALL_RANKED_INDENT || 15,
    DEFAULT_USER_IMAGE_PATH: '/assets/img/user-01.svg',
    FEEDBACK_URL: process.env.FEEDBACK_URL || 'https://forms.gle/nhNhW7GMQ3TPYh8H7',
    IMAGES_HOST: process.env.IMAGES_HOST || '/api/v1'
};
