import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Style from './index.module.css';

const Button = ({ id, onClick, title, active }) => (
  <div
    className={classNames(Style.button, { [Style.active]: active === id })}
    id={id}
    onClick={onClick}
  >
    {title}
  </div>
);

Button.propTypes = {
  active: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default memo(Button);
