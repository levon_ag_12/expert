import { createBrowserHistory } from 'history';

const history = createBrowserHistory();
export default history;
export const goToPage = (loc) => {
  history.push(loc);
};
