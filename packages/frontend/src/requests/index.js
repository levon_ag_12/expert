import querystring from 'querystring';

import APIClient from './APIClient';
import config from '../config';


export const saveExpert = async body => {
  const url = `${config.BACKEND_HOST}/expert`;

  const { data } = await APIClient.post(url, body);
  return data;
};

export const getExpert = async () => {
  const url = `${config.BACKEND_HOST}/expert`;

  const { data } = await APIClient.get(url);
  return data;
};

export const getExpertQuestion = async (req) => {
  const url = `${config.BACKEND_HOST}/expert-question?${querystring.stringify(req)}`;

  const { data } = await APIClient.get(url);
  return data;
};

export const getXLSX = async () => {
  const url2 = `${config.BACKEND_HOST}/answers2`;
  const url3 = `${config.BACKEND_HOST}/answers3`;

  const result2 = await APIClient.get(url2);
  const result3 = await APIClient.get(url3);

  return { ...result2.data, ...result3.data };
}
