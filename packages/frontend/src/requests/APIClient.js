import axios from 'axios';

const getHeaders = (headers = {}) => ({
  'Content-Type': 'application/json',
  ...headers
});

export default {
  get: (url, config = {}) => axios.get(url, {
    headers: getHeaders(),
    ...config,
  }),
  post: (url, data = null, config = {}) => axios.post(url, data, {
    headers: getHeaders(),
    ...config,
  }),
};
