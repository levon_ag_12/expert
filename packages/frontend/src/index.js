/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { Route, Router } from 'react-router';
import { applyMiddleware, compose, createStore } from 'redux';
import ReactDOM from 'react-dom';

import Main from './pages/Main';
import Learn from './pages/Learn';
import Find from './pages/Find';
import history from './history';
import reducers from './redux/reducers';
import sagas from './redux/sagas';
import Lab2 from './pages/Lab2';

require('es6-promise').polyfill();

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, composeEnhancers(
  applyMiddleware(sagaMiddleware)
));

sagaMiddleware.run(sagas);

ReactDOM.render(
  <Provider store={store}>
    <Main />
    <Router history={history}>
      <div>
        <Route
          component={Learn}
          path="/learn"
        />
        <Route
          component={Find}
          path="/find"
        />
        <Route
          component={Lab2}
          path="/lab2"
        />
      </div>
    </Router>
  </Provider>,
  document.getElementById('root')
);
