import React, { memo, useState } from 'react';

import { BUTTON } from '../../constants';
import history, { goToPage } from '../../history';
import Button from '../../components/Button';
import classNames from 'classnames';

import Style from './index.module.css';

const Main = () => {
  const [active, setActive] = useState(() => {
    const current = Object.values(BUTTON).find(item => item.url === history.location.pathname);
    return current ? current.id || '/find' : '/find';
  });

  const goToFindPage = (event) => {
    setActive(event.target.id);
    goToPage(BUTTON.find.url);
  };
  const goToLearnPage = (event) => {
    setActive(event.target.id);
    goToPage(BUTTON.learn.url);
  };
  const goToLab2Page = (event) => {
    setActive(event.target.id);
    goToPage(BUTTON.lab2.url);
  };
  return (
    <div
      className={classNames(Style.container)}
    >
      <Button
        active={active}
        id={BUTTON.find.id}
        onClick={goToFindPage}
        title={BUTTON.find.title}
      />
      <Button
        active={active}
        id={BUTTON.learn.id}
        onClick={goToLearnPage}
        title={BUTTON.learn.title}
      />
      <Button
        active={active}
        id={BUTTON.lab2.id}
        onClick={goToLab2Page}
        title={BUTTON.lab2.title}
      />
    </div>
  );
};

export default memo(Main);
