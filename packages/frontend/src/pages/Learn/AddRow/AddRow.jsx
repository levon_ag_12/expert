/* eslint-disable react/jsx-max-depth */
import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { SUBMIT_BUTTON } from '../../../constants';

import Style from './index.module.css';

const AddRow = ({ addRow }) => {
  const [active, setActive] = useState(false);
  const [newRow, setNewRow] = useState('');

  const handleCLick = () => {
    setActive(true);
  };

  const handleSave = () => {
    addRow(newRow || 'no Name');
    setNewRow('');
    setActive(false);
  };

  const handleCancel = () => {
    setActive(false);
    setNewRow('');
  };

  const handleChange = (event) => {
    setNewRow(event.target.value);
  };

  return active ? (
    <div>
      <div>
        <button
          onClick={handleSave}
          type="submit"
        >
          {SUBMIT_BUTTON.save}
        </button>
        <button
          onClick={handleCancel}
          type="submit"
        >
          {SUBMIT_BUTTON.cancel}
        </button>
      </div>
      <div>
        <input
          onChange={handleChange}
        />
      </div>
    </div>
  ) : (
    <div
      className={classNames(Style.close)}
      onClick={handleCLick}
    />
  );
};

AddRow.propTypes = {
  addRow: PropTypes.func.isRequired,
};


export default memo(AddRow);
