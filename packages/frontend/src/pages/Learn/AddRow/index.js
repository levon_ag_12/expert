import { connect } from 'react-redux';

import AddRow from './AddRow';
import { addRow } from '../../../redux/actions';

const mapDispatchToProps = {
  addRow,
};

export default connect(null, mapDispatchToProps)(AddRow);
