/* eslint-disable react/jsx-max-depth */
import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { SUBMIT_BUTTON } from '../../../constants';

import Style from './index.module.css';

const AddColumn = ({ addColumn }) => {
  const [active, setActive] = useState(false);
  const [newColumn, setNewColumn] = useState('');

  const handleCLick = () => {
    setActive(true);
  };

  const handleSave = () => {
    addColumn(newColumn || 'no Name');
    setNewColumn('');
    setActive(false);
  };

  const handleCancel = () => {
    setActive(false);
    setNewColumn('');
  };

  const handleChange = (event) => {
    setNewColumn(event.target.value);
  };

  return active ? (
    <div>
      <div>
        <button
          onClick={handleSave}
          type="submit"
        >
          {SUBMIT_BUTTON.save}
        </button>
        <button
          onClick={handleCancel}
          type="submit"
        >
          {SUBMIT_BUTTON.cancel}
        </button>
      </div>
      <div>
        <input
          onChange={handleChange}
        />
      </div>
    </div>
  ) : (
    <div
      className={classNames(Style.close)}
      onClick={handleCLick}
    />
  );
};

AddColumn.propTypes = {
  addColumn: PropTypes.func.isRequired,
};


export default memo(AddColumn);
