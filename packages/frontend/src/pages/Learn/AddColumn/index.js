import { connect } from 'react-redux';

import AddColumn from './AddColumn';
import { addColumn } from '../../../redux/actions';

const mapDispatchToProps = {
  addColumn,
};

export default connect(null, mapDispatchToProps)(AddColumn);
