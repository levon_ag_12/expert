
import { connect } from 'react-redux';

import Learn from './Learn';
import {
  getExpert,
  changeCheckBox,
  deleteRow,
  deleteColumn,
  saveExpert,
} from '../../redux/actions';

const mapStateToProps = (state) => ({
  table: state.expert,
});

const mapDispatchToProps = {
  changeCheckBox,
  deleteColumn,
  deleteRow,
  getExpert,
  saveExpert,
};

export default connect(mapStateToProps, mapDispatchToProps)(Learn);
