/* eslint-disable react/jsx-max-depth */
import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import AddColumn from './AddColumn';
import AddRow from './AddRow';
import { SUBMIT_BUTTON } from '../../constants';

import Style from './index.module.css';

const Learn = ({ changeCheckBox, getExpert, table = {}, deleteColumn, deleteRow, saveExpert }) => {
  useEffect(() => {
    getExpert();
  }, [getExpert]);

  const handleCLickCheckBox = event => {
    const [key, value] = event.target.id.split('-');
    changeCheckBox({ key, value });
  };

  const handleDeleteColumn = event => {
    deleteColumn(event.target.id);
  };

  const handleDeleteRow = event => {
    deleteRow(event.target.id);
  };

  const handleSaveExpert = () => {
    saveExpert(table);
  };

  const species = Object.keys(table) || [];
  const params = Object.keys(table[species[0]] || {});

  return table ? (
    <div>
      <table
        cellPadding="5"
        className={classNames(Style.table)}
      >
        <thead >
          <tr>
            <td />
            {params.map(item => (
              <td
                className={classNames(Style.hoverRed)}
                id={item}
                key={item}
                onClick={handleDeleteColumn}
              >
                {item}
              </td>
            ))}
            <td>
              <AddColumn />
            </td>
          </tr>
        </thead>
        <tbody>
          {Object.entries(table).map(([key, value]) => (
            <tr key={`${key}-row`}>
              <th
                className={classNames(Style.hoverRed)}
                id={key}
                key={key}
                onClick={handleDeleteRow}
              >
                {key}
              </th>
              {params.map(item => (
                <th key={`${key}-${item}`}>
                  <input
                    checked={value[item] || false}
                    id={`${key}-${item}`}
                    onChange={handleCLickCheckBox}
                    type="checkbox"
                  />
                </th>
              ))}
            </tr>
          ))}
          <tr>
            <th>
              <AddRow />
            </th>
          </tr>
        </tbody>
      </table>
      <button
        onClick={handleSaveExpert}
        type="submit"
      >
        {SUBMIT_BUTTON.save}
      </button>
    </div>
  ) : null;
};

Learn.propTypes = {
  changeCheckBox: PropTypes.func.isRequired,
  deleteColumn: PropTypes.func.isRequired,
  deleteRow: PropTypes.func.isRequired,
  getExpert: PropTypes.func.isRequired,
  saveExpert: PropTypes.func.isRequired,
  table: PropTypes.objectOf(PropTypes.any),
};


export default memo(Learn);
