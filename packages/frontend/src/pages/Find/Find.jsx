/* eslint-disable max-lines-per-function */
/* eslint-disable no-param-reassign */
/* eslint-disable no-nested-ternary */
import React, { useEffect, memo, useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { getQuestionFromTable } from './methods';

import { SUBMIT_BUTTON } from '../../constants';

import Style from './index.module.css';

const Find = ({ getExpert, table }) => {
  const [question, setQuestion] = useState('');
  const [start, setStart] = useState(false);
  const [result, setResult] = useState(false);
  const [base, setBase] = useState({});
  useEffect(() => {
    getExpert();
  }, [getExpert]);

  const handleStart = () => {
    setStart(true);
    setBase(table);
    setQuestion(getQuestionFromTable(table));
  };

  const handleReset = () => {
    setStart(false);
    setResult(false);
    setQuestion('');
    setBase({});
  };

  const handleYesAnswer = () => {
    const keys = Object.keys(base);
    const newBase = keys.reduce((accum, value) => {
      if (base[value][question]) {
        const params = Object.keys(base[value]).filter(item => item !== question);
        accum[value] = params.reduce((acc, val) => {
          acc[val] = base[value][val];
          return acc;
        }, {});
      }
      return accum;
    }, {});
    if (Object.keys(newBase).length === 1) {
      setResult(Object.keys(newBase)[0]);
    } else {
      setBase(newBase);
      setQuestion(getQuestionFromTable(newBase));
    }
  };

  const handleNoAnswer = () => {
    const keys = Object.keys(base);
    const newBase = keys.reduce((accum, value) => {
      if (!base[value][question]) {
        const params = Object.keys(base[value]).filter(item => item !== question);
        accum[value] = params.reduce((acc, val) => {
          acc[val] = base[value][val];
          return acc;
        }, {});
      }
      return accum;
    }, {});
    if (Object.keys(newBase).length >= 2) {
      setBase(newBase);
      setQuestion(getQuestionFromTable(newBase));
    } else {
      setResult(Object.keys(newBase)[0]);
    }
  };

  return start ? result ? (
    <div>
      <div
        className={classNames(Style.result)}
      >
        {result}
      </div>
      <div>
        <button
          className={classNames(Style.button)}
          id={SUBMIT_BUTTON.reset}
          onClick={handleReset}
          type="submit"
        >
          {SUBMIT_BUTTON.reset}
        </button>
      </div>
    </div>
  ) : (
    <div className={classNames(Style.container)} >
      <div
        className={classNames(Style.question)}
      >
        {`It have ${question}? `}
      </div>
      <div>
        <button
          className={classNames(Style.button)}
          id={SUBMIT_BUTTON.yes}
          onClick={handleYesAnswer}
          type="submit"
        >
          {SUBMIT_BUTTON.yes}
        </button>
        <button
          className={classNames(Style.button)}
          id={SUBMIT_BUTTON.no}
          onClick={handleNoAnswer}
          type="submit"
        >
          {SUBMIT_BUTTON.no}
        </button>
      </div>
    </div>
  ) : (
    <div>
      <button
        className={classNames(Style.button)}
        id={SUBMIT_BUTTON.start}
        onClick={handleStart}
        type="submit"
      >
        {SUBMIT_BUTTON.start}
      </button>
    </div>
  );
};

Find.propTypes = {
  getExpert: PropTypes.func.isRequired,
  table: PropTypes.objectOf(PropTypes.any),
};


export default memo(Find);
