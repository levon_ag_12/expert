
import { connect } from 'react-redux';

import Find from './Find';
import {
  getExpert,
} from '../../redux/actions';

const mapStateToProps = (state) => ({
  table: state.expert,
});

const mapDispatchToProps = {
  getExpert,
};

export default connect(mapStateToProps, mapDispatchToProps)(Find);
