/* eslint-disable id-length */
/* eslint-disable no-plusplus */
export const getQuestionFromTable = (table) => {
  const badMatrix = Object.values(table).map((item) => Object.values(item));
  const names = Object.keys(table);
  const values = Object.keys(table[names[0]]);
  const matrix = [];
  for (let i = 0; i < badMatrix[0].length; i++) {
    matrix[i] = badMatrix.map(item => item[i]);
  }

  // state 1
  const newValues = [...values];
  const newMatrix = matrix.filter((item, i) => {
    if (item.includes(true)) {
      return true;
    }
    newValues.splice(i, 1);
    return false;
  });

  // state 2
  const sums = newMatrix.map(item => item.reduce((acc, val) => acc + val));
  const sortedSums = [...sums].sort((a, b) => a - b);
  const number = sums.indexOf(sortedSums[0]);

  // state 3
  const question = values[number] || 'noting';
  return question;
  // await state1(base, matrix, values);
};
