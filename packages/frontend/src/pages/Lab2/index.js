
import { connect } from 'react-redux';

import Lab2 from './Lab2';
import {
  getXLSX
} from '../../redux/actions';

const mapStateToProps = (state) => ({
  xlsx: state.xlsx,
});

const mapDispatchToProps = {
  getXLSX,
};

export default connect(mapStateToProps, mapDispatchToProps)(Lab2);
