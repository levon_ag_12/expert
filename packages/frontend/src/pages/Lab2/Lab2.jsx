/* eslint-disable react/jsx-max-depth */
import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import classNames from 'classnames';

import Style from './index.module.css';

const getSorted = (xlsx) => {
  const ILLNESS = 'illness';
  const HEALTHY = 'healthy';
  const arr = xlsx[ILLNESS].map((x, i) => {
    const badAss = x.slice(1);
    const goodAss = xlsx[HEALTHY][i].slice(1);
    const badMin = Math.min(...badAss);
    const goodMax = Math.max(...goodAss);
    const good = goodAss.reduce((acc, val) => {
      if (val < badMin) {
        return acc + 1;
      }
      return acc;
    }, 0)
    const bad = badAss.reduce((acc, val) => {
      if (val > goodMax) { return acc + 1; }
      return acc;
    }, 0)
    const value = bad + good;
    return [x[0], value];
  });
  return arr.sort((a, b) => a[1] - b[1])

};

const Lab2 = ({ xlsx, getXLSX }) => {
  useEffect(() => {
    getXLSX();
  }, [getXLSX]);
  
  return !isEmpty(xlsx) ? (
    <div>
     {getSorted(xlsx).map(x => (
       <div
       className={classNames(Style.container, {[Style.isGood]: x[1] > 9})}
        key={x[0]}
       >
         <div
          className={classNames(Style.number)}
         >
          {x[1]}
         </div>
         <div
         className={classNames(Style.text)}
         >
         {x[0]}
         </div>
       </div>
     ))}
    </div>
  ) : '';
};

Lab2.propTypes = {
  getXLSX: PropTypes.func.isRequired,
  xlsx: PropTypes.shape({}),
};

export default memo(Lab2);
