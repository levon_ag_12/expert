# expert + lab2

### clone project

```
git clone https://levon_ag_12@bitbucket.org/levon_ag_12/expert.git
```

### install oll dependensies

```
cd packages/backend
yarn or npm install
cd ../frontend
yarn or npm install
```

## to Run project follow this steps

### to run backend run commands and don't close terminal

```
cd packages/backend/src
node index.js

```

### to run frontend

#### open another terminal and run commands

```
cd packages/frontend
yarn start or npm run start
```